# Précisions sur des notions déjà étudiées ?

## Bonjour le Monde

Pas très original mais c'est la  tradition depuis 1970 alors voici notre premier
programme Python:

```shell
$ echo "print('Hello World')" > PremierProg.py 

$ python PremierProg.py 
Hello World
```

Que s'est-il passé?

```mermaid
graph LR
A["éditeur"]--> |PremierProg.py|B{"compilateur"};
B--> |bytecode|C{"interpréteur"};
C--> |Hello World|D["sortie standard"] ;
```


## Un peu de vocabulaire

### Exemple introductif

Quand on écrit:

```python
var1 = "Point n'est besoin d'espérer"
var2 = " pour entreprendre"
var3 = var1 + var2
```
On crée trois **objets** de **type** `str`, *"Point n'est besoin d'espérer"* et 
*"pour  entreprendre"*  sont deux  **constantes**  (*littéraux*  en franglais)  et
`var1  + var2`  une **expression**.  Cela permet  de **lier**  les **variables**
`var1`, `var2`  et `var3`  à ces  constantes ou expressions.  On fait  ainsi des
**affectations**.


**Remarque**


En Python, une expression est évaluée avant d'être affectée.

Par exemple en Python:
```python
>>> a = 0
>>> b = 2
>>> c = (1, b/a)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: division by zero
```
Alors que dans un autre langage (Haskell ici):

```haskell
Prelude> a = 0
Prelude> b = 2
Prelude> c = (1, b/a)
```

Ce n'est que lors de l'appel de la variable que l'expression est évaluée:

```haskell
Prelude> c
(1,Infinity)
```

*Énigme*

Comment expliquer ceci en Bash 

```shell
$ a=0
$ b=2
$ c=(1,$((b/a)))
bash: b/a : division par 0 (le symbole erroné est « a »)
$ c=(1,$b/$a)
$ echo $c
1,2/0
```

### Constante

C'est la représentation  en code Python d'une valeur d'un  certain type. `1` est
une constante de type `int`, `True` est une constante de type `bool`, etc.

### Opérateur

C'est  la représentation  en code  Python d'une  opération sur  des objets  d'un
certain type : par exemple `+` est l'opérateur représentant l'opération *addition des `int`* mais c'est
aussi l'opérateur représentant l'opération *concaténation de deux `str`*.



### Identifiant

C'est la représentation  en code Python d'un  nom qui est associé  à une adresse
mémoire. Les  identifiants sont des suites  de lettres, de chiffres  et de tiets
bas, le premier caractère ne pouvant être un chiffre.

Attention à  ne pas choisir  comme identifiant des  mots-clés ou de  types comme
`while`, `int`, etc.

### Variable

Une variable est  un nom associé à une valeur  pour faciliter son identifiaction
par le programmeur et dont la valeur peut changer au cours du programme. 

### Expression

Une expression est une combinaison  de constantes, variables et d'opérateurs que
Python va *évaluer*, i.e. Python va produire une valeur associée à cette expression.


### Affectation

Quand on  écrit `d =  1` en Python, on  n'exprime pas une  identité mathématique
mais une **action** qui consiste à :

* définir un *identifiant* `d` comme une nouvelle *variable*
* associer à cette variable un entier dont la *valeur* est 1.


```mermaid
graph TD;
A("label d")-->B["int 1"]
```




On fait bien cette distinction dans d'autres langages comme `OCaml`:

```ocaml
# let d = ref 1;;
val d : int ref = {contents = 1}

# d;;
- : int ref = {contents = 1}

# !d;;
- : int = 1
```

Quand on écrit:

```python
a = 12
t = a
```

on a  deux variables `a` et  `t` qui pointent vers  le même objet `12`  (qui est
ici un `int`).

En `bash`, on connaît l'usage du `$` pour travailler avec la valeur d'une référence:

```bash
message='Bonjour'
echo $message
```


### Objet

Toutes les données  d'un programme Python sont des objets  qui sont caractérisés
par:

* leur **identifiant**:  en gros, cela indique un numéro  qui identifie une case
  mémoire.
  
* leur **type**:  cela permet de connaître le comportement  de l'objet, ce qu'il
  peut représenter et ce qu'on peut lui faire faire.
  
* leur **valeur**: la donnée qu'il représente.


### Référence d'un objet

En  fait,  les  noms qu'on  donne  aux  objets  sont  juste des  liens  vers  les
objets. Écrire `a = b` signifie qu'on copie l'adresse `a` dans `b`. 

La fonction `id`  nous permet d'avoir un *identifiant* unique  pour un objet qui
sera  unique  tant  que  cet  objet restera  "vivant"  (la  plupart  des  objets
disparaissent dans le *ramasse-miettes*).  C'est comme un numéro de
sécurité sociale.





**Observez, commentez, expliquez**

```python
>>> a = 1                                                            

>>> b = a                                                            

>>> id(a)                                                            
 93835776299776

>>> id(b)                                                            
 93835776299776
```


```mermaid
graph TD;
  a("a")-->1;
  b("b")-->1;
```




```python
>>> b = 2 

>>> id(b)                                                            
 93835776299808

>>> id(a)                                                            
 93835776299776
```



```mermaid
graph TD;
  a("a")-->1;
  b("b")-->2;
```


```python
>>> a = b

>>> id(a)                                                            
 93835776299808
```
  


```mermaid
graph TD;
	       1;
  a("a")-->2;
  b("b")-->2;
```



## Et dans d'autres langages ?

Certains langages  refusent la  ré-affectation. Par  exemple `Rust`,  le langage
lancé par  Mozilla qui  sert à  développer Firefox et  qui est  de plus  en plus
utilisé par les développeurs:

```rust
let x = 1;
x = x + 1;
```

En compilant on obtient:

```rust
error[E0384]: cannot assign twice to immutable variable `x`
 --> /tmp/r.rs:1:24
  |
1 |             let x = 1; x = x + 1; 
  |                 -      ^^^^^^^^^ cannot assign twice to immutable variable
  |                 |
  |                 first assignment to `x`

error: aborting due to previous error

```



D'autres langages n'ont carrément pas d'affectation comme Haskell qui a inspiré
de nombreux langages parmi les plus utilisés actuellement. 

On ne peut pas écrire `a = a + 1`.

Nous en  rediscuterons plus tard  mais on essaiera  de recourir le  plus souvent
possible aux **affectations uniques**.

On évitera les `a = a + a` qui rendent la lecture du code difficile.

On préfèrera utiliser une nouvelle variable.


Quant à l'utilisation du symbole `=` pour l'affectation, voici ce qu'en dit Niklaus Wirth

*A notorious  example for a  bad idea  was the choice  of the equal  sign to
    denote assignment. It  goes back to Fortran in 1957[a]  and has blindly been
    copied by  armies of language  designers. Why is it  a bad idea?  Because it
    overthrows  a century  old  tradition to  let “=”  denote  a comparison  for
    equality, a predicate which is either true  or false. But Fortran made it to
    mean assignment, the  enforcing of equality. In this case,  the operands are
    on unequal footing: The left operand (a variable) is to be made equal to the
    right operand (an expression). x = y does not mean the same thing as y = x.*
	
	
	




### On retiendra
 
>En python, on peut lire `a = 1` comme  *`a` est lié à un objet de type `int` dont la valeur est 1*.
>
> On peut même préciser `a:int = 1`
>
>En Python, on copie des adresses, pas des valeurs






