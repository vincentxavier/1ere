# Structures de données vite fait

Nous  aurons besoin  de parcourir  des "groupes"  de données  qui peuvent  avoir
différentes propriétés  selon nos  besoins. Nous  allons rapidement  parler d'un
premier lot sans trop détailler et irons  plus loin un peu plus tard cette année
et l'an prochain.

Nous allons surtout les découvrir telles  que `Python` nous les fournit et c'est
assez spécifique.

## Listes...Python (`List`)

Les listes `Python` de type `List` ne  sont pas des listes au sens algorithmique
du terme mais pour l'instant nous ne nous en occuperons pas.

**Une  liste `Python`  est une  structure *ordonnée*  et *mutable*  et dont  les
éléments sont de type *homogène* (autant que faire ce peut)...**

Les éléments d'une liste sont numérotés de  "gauche" à "droite" à partir de 0 et
de "droite" à "gauche" à partir de -1:

```
 -5 -4 -3 -2 -1  

[ a, b, c, d, e ]

 0  1   2  3  4
```

Par exemple, créons une liste de chaînes de caractères:

```python
>>> from typing import List
>>> xs: List[str] = ['Papa', 'Maman', 'La bonne', 'Moi']
>>> xs
['Papa', 'Maman', 'La bonne', 'Moi']
>>> xs[0]
'Papa'
>>> xs[1]
'Maman'
>>> xs[2]
'La bonne'
>>> xs[3]
'Moi'
>>> xs[-1]
'Moi'
>>> xs[-2]
'La bonne'
>>> xs[-3]
'Maman'
>>> xs[-4]
'Papa'
>>> xs[-5]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
>>> xs[4]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
```

`len` comme *LENgth* nous permet d'avoir accès à la longueur de la liste *i.e.* le nombre de ses éléments:



```python
>>> len(xs)
4
```


On peut extraire des **sous-listes** d'une liste (ou *tranche* ou *slice*) à l'aide de la syntaxe:

```
xs[indice début : indice fin]
```

Attention ! L'indice de fin est non compris.

```python
>>> xs[1:3]
['Maman', 'La bonne']
```

Si l'on veut aller jusqu'à la fin, on n'écrit rien à droite des `:`:

```python
>>> xs[1:]
['Maman', 'La bonne', 'Moi']
```

Si l'on veut partir du début, on ne met rien à gauche des `:`:

```python
>>> xs[:3]
['Papa', 'Maman', 'La bonne']
```

Expliquez:

```python
>>> xs[1:-1]
['Maman', 'La bonne']
```

### Concaténation - Mutabilité

On peut "coller" des listes ou plutôt les **concaténer** avec l'opérateur `+`

```python
>>> xs + ['Ma soeur']
['Papa', 'Maman', 'La bonne', 'Moi', 'Ma soeur']
```

```python
>>> ['Odin'] + xs
['Odin', 'Papa', 'Maman', 'La bonne', 'Moi']
```

Une liste Python étant *mutable*, on peut la modifier:

```python
>>> xs = ['Odin'] + xs
>>> xs
['Odin', 'Papa', 'Maman', 'La bonne', 'Moi']
>>> xs = xs[1:]
>>> xs
['Papa', 'Maman', 'La bonne', 'Moi']
```

On peut utiliser une fonction spécifique aux listes, qui n'existe que pour les listes, et dont la syntaxe est donc différente : si on veut ajouter un élément à la fin, on utilise `append`:

```python
>>> xs.append('Ma soeur')
>>> xs
['Papa', 'Maman', 'La bonne', 'Moi', 'Ma soeur']
```

Il y a deux choses à noter :

* `append` ne renvoie rien, ou plutôt renvoie `None` mais, "en douce" modifie la liste et lui ajoute un élément à la fin. Nous en reparlerons dans notre section sur les focntions.

* Pour appliquer `append` à la liste, on écrit `liste.append(élément)`. Nous comprendrons plus tard pourquoi : cela vient de l'orientation "Objet" de la programmation en `Python`.


### test d'appartenance

Ce sera souvent utile:

```python
>>> xs = [1, 2, 3]
>>> 2 in xs
True
>>> 4 in xs
False
```


### Les listes et la mémoire

Observez, c'est étrange...

```python
>>> xs = [1,2,3]
>>> id(xs)
140077907522656
>>> id(xs[0])
93990234841856
>>> ys = xs
>>> id(ys)
140077907522656
>>> xs.append(4)
>>> xs
[1, 2, 3, 4]
>>> ys
[1, 2, 3, 4]
>>> ys = ys + [5]
>>> xs
[1, 2, 3, 4]
>>> id(xs)
140077907522656
>>> id(ys)
140077906785616
>>> zs = xs[:]
>>> id(zs)
140077906700240
>>> zs
[1, 2, 3, 4]
>>> xs
[1, 2, 3, 4]
>>> ys
[1, 2, 3, 4, 5]
```

Regardez comment ça se passe sur [Pythontutor](http://pythontutor.com/visualize.html#code=xs%20%3D%20%5B1,2,3%5D%0Ays%20%3D%20xs%0Axs.append%284%29%0Ays%20%3D%20ys%20%2B%20%5B5%5D%0Azs%20%3D%20xs%5B%3A%5D&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) :

[![](./IMG/pythontutor.png)](http://pythontutor.com/iframe-embed.html#code=xs%20%3D%20%5B1,2,3%5D%0Ays%20%3D%20xs%0Axs.append%284%29%0Ays%20%3D%20ys%20%2B%20%5B5%5D%0Azs%20%3D%20xs%5B%3A%5D&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false")



## Les n-uplets (`Tuple`)


Les n-uplets (`Tuples`) de Python, c'est presque pareil, sauf que...


**Un `Tuple` `Python` est une structure *ordonnée* et *NON mutable* et dont les
éléments sont de type *homogène OU NON*...**


Donc  on favorisera  l'utilisation de  `Tuples` lorsqu'on  voudra manipuler  des
objets de types divers et qui n'ont pas vocation à évoluer.


On peut cependant faire évoluer des `Tuples` par concaténation :

```python
>>> (1,2) + (3,4)
(1, 2, 3, 4)
```

Mais  comment ajouter  un  seul élément  ?  Pour `Python`,  `(1)`  n'est pas  un
`Tuple`, c'est juste un 1 entre parenthèse :)


```python
>>> type((1))
<class 'int'>
```

Ruse ! On écrit `(1,)`

```python
>>> type((1,))
<class 'tuple'>
```

Ainsi:

```python
>>> (1,2) + (3)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate tuple (not "int") to tuple
>>> (1,2) + (3,)
(1, 2, 3)
```

Bref, tout s'explique quand on cherche à comprendre !



```python

```

### Test d'appartenance

```python
>>> t = (1, 'a', 12.3)
>>> 1 in t
True
```


### Tuple assignment

C'est souvent très pratique

```python
>>> (x, y) = (1, 3)
>>> x
1
>>> y
3
```


On  peut  même **dans  ce  cas**  se passer  de  parenthèses,  qui seront  mises
automatiquement :

```python
>>> x, y = 1, 3
>>> x
1
>>> y
3

```


Observez et expliquez :

```python
>>> x, y = 1, 2
>>> x
1
>>> y
2
>>> x, y = y, x
>>> x
2
>>> y
1
>>> x, y = y, x + y
>>> x
1
>>> y
3
```


## Les ensembles (`Set`)



**Un  **ensemble** `Python`  (`Set`) est  une structure  *NOΝ ordonnée*  et *NON
mutable* et dont les éléments sont de type *homogène OU NON* et SANS RÉPÉTITIONS...**


```python
>>> s1 = {1, 2, 3}
>>> s2 = {2, 3, 1}
>>> s3 = {1, 3, 1, 2, 2, 3}
>>> s1 == s2
True
>>> s1 == s3
True
>>> s1
{1, 2, 3}
>>> s2
{1, 2, 3}
>>> s3
{1, 2, 3}
```

### Opérations

On a l'union : `|`

```python
>>> s1 = {1, 2, 3}
>>> s2 = {2, 3, 4, 5}
>>> s1 | s2
{1, 2, 3, 4, 5}
```

On a l'intersection

```python
>>> s1 & s2
{2, 3}
```

On a l'appartenance

```python
>>> 2 in {1, 2, 3}
True
```

et l'inclusion:

```python
>>> {1, 2} < {1, 2, 3}
True
>>> {1, 2} <= {1, 2}
True
>>> {1, 2} < {1, 2}
False
```

et la différence (on enlève les éléments du second dans le premier):

```python
>>> {1, 2, 3} - {2, 3, 4}
{1}
>>> {2, 3, 4} - {1, 2, 3}
{4}
```


## `range`

C'est un objet un peu à part...Voyons l'aide :


```console
Help on class range in module builtins:

class range(object)
 |  range(stop) -> range object
 |  range(start, stop[, step]) -> range object
 |  
 |  Return an object that produces a sequence of integers from start (inc
lusive)
 |  to stop (exclusive) by step.  range(i, j) produces i, i+1, i+2, ..., 
j-1.
 |  start defaults to 0, and stop is omitted!  range(4) produces 0, 1, 2,
 3.
 |  These are exactly the valid indices for a list of 4 elements.
 |  When step is given, it specifies the increment (or decrement).
 |  
```

```python
>>> r = range(1, 5)
>>> 1 in r
True
>>> 0 in r
False
>>> 4 in r
True
>>> 5 in r
False
>>> 2.5 in r
False
```
