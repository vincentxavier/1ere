import easygui as eg
import os
import sys

encore: bool = True
while encore:
    ville: str = eg.enterbox('Ville ?', default='')
    os.system(f'wget -O {ville}.txt https://fr.wttr.in/{ville}?format="%l:+%C+%t+%p+%w"')
    temps: str = open(f'{ville}.txt','r').read()
    tps: str = temps.split()[1]
    logo: str = f'{tps}.png'
    os.system(f'rm {ville}.txt')
    encore = eg.ynbox(msg=temps, choices=['Une autre ville ?','Quitter'], image=logo, title=f'Météo de {ville}')
