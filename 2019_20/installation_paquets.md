# Comment installer des paquets dans son HOME sans être SU ?


Il suffit d'installer `junest`

```console
$ mkdir -p ~/.local/share/
$ cd ~/.local/share/
$ wget https://github.com/fsquillace/junest/archive/master.zip
$ unzip master.zip
$ echo 'PATH=~/.local/share/junest-master/bin:$PATH' >> ~/.bashrc
```

et ensuite pour installer le paquet `tk` par exemple:

```console
$ ~/.local/share/junest-master/bin/junest setup
$ ~/.local/share/junest-master/bin/junest p -f
# pacman -Syy
# pacman -S tk python-pip gcc libgl
# pip install --upgrade pip
# pip3 install easygui spyder jupyter
# jupyter notebook --allow-root
```

Vous  faites   un  clic  droit  sur   l'url  proposée  et  vous   l'ouvrez  dans
`firefox`. Vous êtes alors dans `Jupyter` et pouvez lancer une nouvelle session `python`.
